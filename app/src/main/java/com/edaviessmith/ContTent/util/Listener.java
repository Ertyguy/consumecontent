package com.edaviessmith.contTent.util;

public interface Listener {
    public void onComplete(String value);
    public void onError(String value);
}